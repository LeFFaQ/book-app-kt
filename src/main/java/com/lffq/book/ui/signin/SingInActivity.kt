package com.lffq.book.ui.signin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.lffq.book.R
import com.lffq.book.databinding.ActivitySingInBinding
import com.lffq.book.ui.singup.SingUpActivity

class SingInActivity : AppCompatActivity() {

    lateinit var binding: ActivitySingInBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel: SingInViewModel by viewModels()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sing_in)
        binding.viewmodel = viewModel
    }

    fun toUp(view: View) {
        startActivity(Intent(this,SingUpActivity::class.java))
    }

}