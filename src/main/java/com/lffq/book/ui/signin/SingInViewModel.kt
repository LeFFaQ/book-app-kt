package com.lffq.book.ui.signin

import android.content.ContentValues.TAG
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth


class SingInViewModel: ViewModel() {

    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    var email: String = ""
    var password: String = ""
    var rememberme: Boolean = false


    fun forgetPassword(view: View) {
        Log.d(TAG, "forgetPassword: лол ты че пароль забыл")
    }

    fun SingInView(view: View) {
        SingIn(email, password)
    }


    fun SingIn(email: String, password: String) {

        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = mAuth.currentUser

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                }

                // ...
            }
    }

}