package com.lffq.book.ui.singup

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.lffq.book.R
import com.lffq.book.databinding.ActivitySingUpBinding

class SingUpActivity : AppCompatActivity() {

    lateinit var binding: ActivitySingUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel: SingUpViewModel by viewModels()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sing_up)
        binding.viewmodel = viewModel


    }

    fun toBack(view: View) {finish()}

}