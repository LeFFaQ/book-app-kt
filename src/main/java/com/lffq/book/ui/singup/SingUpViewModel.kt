package com.lffq.book.ui.singup

import android.content.ContentValues.TAG
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase

class SingUpViewModel: ViewModel() {

    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    var mDataBase:FirebaseDatabase = FirebaseDatabase.getInstance()


    var username: String = ""
    var email: String = ""
    var password: String = ""


    fun singUpView(view: View) {
        createUser(email, password, username)
    }


    fun createUser(email: String, password: String, username: String) {
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    addName(username)
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "onCreateUser: successful"  + task.exception)
                    val user: FirebaseUser? = mAuth.currentUser
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d(TAG, "onCreateUser: error")}

            }
    }

    fun addName(username: String) {
        mDataBase.getReference("Users")
            .child(mAuth.currentUser?.uid.toString())
            .setValue(username)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "onAddName: successful")
                } else {
                    Log.d(TAG, "onAddName: error")
                }
            }
    }

}